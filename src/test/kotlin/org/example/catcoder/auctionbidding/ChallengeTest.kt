package org.example.catcoder.auctionbidding

import assertk.assertThat
import assertk.assertions.isEqualTo
import org.approvaltests.Approvals.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.junit.jupiter.params.provider.ValueSource
import kotlin.math.max
import kotlin.math.min

typealias Bid = Pair<String, Int>

data class Input(val startBid: Int, val buyPrice: Int, val bids: List<Bid>)

data class Result(val history: List<Bid>)

const val BID_INCREMENT = 1

class Auction(var price: Int, buyPrice: Int) {
    private var highestBidder = "-"
    private var highestBid = price - 1
    private val buyPrice = buyPrice.takeUnless { it == 0 } ?: Integer.MAX_VALUE
    val history = mutableListOf(highestBidder to price)

    val isOpen: Boolean get() = price < buyPrice

    fun bid(entry: Bid) {
        val (bidder, bid) = entry
        if (bidder == highestBidder) {
            raiseOwnBid(bid)
        } else {
            raiseNewBid(bid, bidder)
        }
    }

    private fun raiseNewBid(
        bid: Int,
        bidder: String,
    ) {
        if (bid < highestBid) {
            price = bid + BID_INCREMENT
        } else if (bid > highestBid) {
            price = highestBid + BID_INCREMENT
            highestBidder = bidder
            highestBid = bid
        } else {
            price = bid
        }

        price = min(price, buyPrice)
        history.add(highestBidder to price)
    }

    private fun raiseOwnBid(bid: Int) {
        highestBid = max(highestBid, bid)
    }
}

class Challenge {
    fun read(input: String): Input {
        val tokens = input.split(",").iterator()

        return Input(
            startBid = tokens.next().toInt(),
            buyPrice = tokens.next().toInt(),
            bids = parseBids(tokens),
        )
    }

    private fun parseBids(tokens: Iterator<String>): MutableList<Bid> {
        val bids = mutableListOf<Bid>()

        while (tokens.hasNext()) {
            bids.add(tokens.next() to tokens.next().toInt())
        }

        return bids
    }

    fun eval(input: Input): Result {
        var auction = Auction(price = input.startBid, buyPrice = input.buyPrice)
        val bids = input.bids.listIterator()

        while (bids.hasNext() && auction.isOpen) {
            auction.bid(bids.next())
        }
        return Result(history = auction.history)
    }

    fun print(result: Result): String {
        return result.history.joinToString(",") { (bidder, price) -> "$bidder,$price" }
    }

    fun repl(input: String): String {
        return input.let(this::read).let(this::eval).let(this::print)
    }
}

class ChallengeTest {
    @ParameterizedTest
    @ValueSource(
        strings = [
            "1,0",
            "2,0",
            "3,0,A,3",
            "4,0,A,4,B,5",
            "6,0,A,7",
            "1,0,A,5,B,5",
        ],
    )
    fun `verify multiple approvals`(input: String) {
        verify("$input => ${Challenge().repl(input)}", NAMES.withParameters(input))
    }

    @ParameterizedTest
    @MethodSource("examples")
    fun `verify some examples`(
        input: String,
        expected: String,
    ) {
        assertThat(Challenge().repl(input)).isEqualTo(expected)
    }

    @Test
    fun `verify level 4`() {
        val output =
            listOf(
                "1,15,A,5,B,10,A,8,A,17,B,17",
                "100,0,C,100,C,115,C,119,C,121,C,144,C,154,C,157,G,158,C,171,C,179,C,194,C,206,C,214,C,227,C,229,C,231,C,298",
                "100,325,C,100,C,115,C,119,C,121,C,144,C,154,C,157,G,158,C,171,C,179,C,194,C,206,C,214,C,227,C,229,C,231,C,298",
                "100,160,C,100,C,115,C,119,C,121,C,144,C,154,C,157,G,158,C,171,C,179,C,194,C,206,C,214,C,227,C,229,C,231,C,298",
                "1,0,nepper,15,hamster,24,philipp,30,mmautne,31,hamster,49,hamster,55,thebenil,57,fliegimandi,59,ev,61,philipp,64,philipp,65,ev,74,philipp,69,philipp,71,fliegimandi,78,hamster,78,mio,95,hamster,103,macquereauxpl,135",
                "1,120,6a,17,kl,5,kl,10,kl,15,cs,28,kl,20,kl,25,hr,35,hr,40,hr,41,hl,42,hr,43,hr,44,hl,44,hl,49,hr,47",
                "1,47,6a,17,kl,5,kl,10,kl,15,cs,28,kl,20,kl,25,hr,35,hr,40,hr,41,hl,42,hr,43,hr,44,hl,44,hl,49,hr,47",
            ).map { input -> "$input => ${Challenge().repl(input)}" }

        verifyAll("level 4", output)
    }

    companion object {
        @JvmStatic
        fun examples() =
            listOf(
                Arguments.of("1,0,A,5,B,10,A,8,A,14,A,17,B,17", "-,1,A,1,B,6,B,9,A,11,A,17"),
                Arguments.of("1,15,A,5,B,10,A,8,A,17,B,17", "-,1,A,1,B,6,B,9,A,11,A,15"),
            )
    }
}
